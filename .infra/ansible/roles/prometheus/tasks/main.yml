---
- name: Install Prometheus
  apt:
    name: prometheus
    state: present
  become: true

- name: Configure Prometheus
  copy:
    content: "{{ prometheus_conf }}"
    dest: /etc/prometheus/prometheus.yml
  become: true

- name: Start Prometheus service
  service:
    name: prometheus
    state: started
  become: true

- name: Enable Prometheus service
  service:
    name: prometheus
    enabled: true
  become: true

- name: Install node_exporter
  get_url:
    url: "https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_version }}/node_exporter-{{ node_exporter_version }}.linux-amd64.tar.gz"
    dest: /tmp/node_exporter.tar.gz
  become: true

- name: Extract node_exporter
  unarchive:
    src: /tmp/node_exporter.tar.gz
    dest: /opt
    remote_src: yes
    extra_opts: [--strip-components=1]
  become: true

- name: Copy node_exporter binary to /usr/local/bin
  shell: cp /opt/node_exporter /usr/local/bin/node_exporter

- name: Create node_exporter service file
  copy:
    content: |
      [Unit]
      Description=Node Exporter
      After=network.target

      [Service]
      User=node_exporter
      Group=node_exporter
      Type=simple
      ExecStart=/usr/local/bin/node_exporter

      [Install]
      WantedBy=multi-user.target
    dest: /etc/systemd/system/node_exporter.service
    mode: '0644'

- name: Enable and start node_exporter service
  systemd:
    name: node_exporter
    state: started
    enabled: true 
    daemon_reload: yes
  become: true

- name: Configure Prometheus to scrape node_exporter
  lineinfile:
    path: /etc/prometheus/prometheus.yml
    line: "  - job_name: 'node_exporter'"
    insertafter: "scrape_configs:"
  become: true

- name: Configure Prometheus to scrape node_exporter metrics
  lineinfile:
    path: /etc/prometheus/prometheus.yml
    line: "    static_configs:"
    insertafter: "  - job_name: 'node_exporter'"
  become: true

- name: Configure Prometheus to scrape node_exporter metrics from localhost
  lineinfile:
    path: /etc/prometheus/prometheus.yml
    line: "      - targets: ['localhost:9100']"
    insertafter: "    static_configs:"
  become: true
