variable "subnet" {
  type = string
}

variable "rebrainkey"{
  type = string
}
  
variable "service" {
  type = string
}

variable "platform" {
  type = list(string)
  default = ["standard-v1"]
}

variable "zone" {
  type = string
}

variable "cloud" {
  type = string
}

variable "folder" {
  type = string
}

variable "sshkey" {
  type = string
}

variable "accesskey"{
  type = string
}

variable "images" {
  type = list(string)
  default = ["fd8g89v5520br3460363"]
}

variable "secretkey" {
  type = string
}

variable "pass" {
  type = string
}

variable "devs" {
  type = list(string)
  default =  ["devops-final-imslttan-at-yandex-ru"]
}


variable "mail" {
  type = string
}

variable "task" {
  type = list(string)
  default = ["devops-final"]
}
