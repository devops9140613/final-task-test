resource "yandex_compute_instance" "vps" {
  count = length(var.devs)
  name = var.devs[count.index]
  platform_id = var.platform[count.index]
  zone = var.zone
  allow_stopping_for_update = true 

  resources {
    core_fraction = 5
    cores  = 2
    memory = 2
  }
  network_interface {
    subnet_id = var.subnet
    nat       = true
  }
  boot_disk {
    initialize_params {
      image_id = var.images[count.index]
      size = 20
    }
  }
  metadata = {
    foo      = "bar"
    ssh-keys = "ubuntu:${file("/root/.ssh/id_ed25519.pub")}"
  }
  labels = {
    user_email = var.mail
    task_name = var.task[count.index]
  }
}
resource "local_file" "nginx" {
  count = length(var.devs)
  content = templatefile ("./file.tmpl",
  {
   webservers = {
names = yandex_compute_instance.vps.*.name 
ips = yandex_compute_instance.vps[0].network_interface[0].nat_ip_address
}
  })
  filename = "../ansible/roles/nginx/vars/var1.yaml"
  }
resource "local_file" "certificate" {
  count = length(var.devs)
  content = templatefile ("./file.tmpl",
  {
   webservers = {
names = yandex_compute_instance.vps.*.name
ips = yandex_compute_instance.vps[0].network_interface[0].nat_ip_address
}
  })
  filename = "../ansible/roles/certificate/vars/var1.yaml"
  }

resource "local_file" "inventory" {
  count = length(var.devs)
  content = templatefile ("./file2.tmpl",
  {
   webservers = {
names = yandex_compute_instance.vps.*.name
ips = yandex_compute_instance.vps[0].network_interface[0].nat_ip_address
}
  })
  filename = "../ansible/inventory.yaml"
  }

