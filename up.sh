#!/bin/bash
# Run terraform
cd .infra/terraform
terraform init
terraform apply -auto-approve

# Wait for SSH to become available
sleep 60
# Run ansible
cd ../ansible
ansible-playbook -i inventory.yaml nginx.yaml -u ubuntu -e 'ansible_python_interpreter=/usr/bin/python3' --vault-password-file=/home/imslttan/testfinal/.ansible-vault_pass
