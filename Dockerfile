# Используем базовый образ Python
FROM python:3.8

# Устанавливаем зависимости для Odoo
RUN apt-get update && apt-get install -y --no-install-recommends \
        python3-dev \
        libxml2-dev \
        libxslt1-dev \
        libldap2-dev \
        libsasl2-dev \
        libssl-dev \
        libpq-dev \
        libjpeg-dev \
        zlib1g-dev \
        npm

# Устанавливаем Odoo
RUN git clone --depth=1 --branch=14.0 https://www.github.com/odoo/odoo /opt/odoo

# Устанавливаем зависимости Python для Odoo
RUN pip install -r /opt/odoo/requirements.txt

# Копируем конфигурационный файл Odoo
COPY ./odoo.conf /etc/odoo/odoo.conf

# Определяем рабочую директорию
WORKDIR /opt/odoo

# Открываем порт для доступа к Odoo
EXPOSE 8069

# Запускаем Odoo
CMD ["./odoo-bin", "-c", "/etc/odoo/odoo.conf"]
